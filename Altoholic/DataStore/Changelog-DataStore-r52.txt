------------------------------------------------------------------------
r52 | thaoky | 2015-01-09 16:59:48 +0000 (Fri, 09 Jan 2015) | 3 lines
Changed paths:
   M /trunk/DataStore.lua
   M /trunk/Options.lua
   M /trunk/Options.xml

- Minor fix in GetMethodOwner()
- Fixed ToggleOption()
- XML frames cleanup
------------------------------------------------------------------------
r51 | thaoky | 2014-12-27 13:07:24 +0000 (Sat, 27 Dec 2014) | 1 line
Changed paths:
   M /trunk/DataStore.lua
   M /trunk/Options.lua

Fixed 'Player not found' spam.
------------------------------------------------------------------------
r49 | thaoky | 2014-12-24 13:41:50 +0000 (Wed, 24 Dec 2014) | 1 line
Changed paths:
   M /trunk/DataStore.toc

.toc update to use project-version
------------------------------------------------------------------------
r48 | thaoky | 2014-10-17 17:01:33 +0000 (Fri, 17 Oct 2014) | 1 line
Changed paths:
   M /trunk/DataStore.lua
   M /trunk/DataStore.toc

6.0.001 Commit
------------------------------------------------------------------------
