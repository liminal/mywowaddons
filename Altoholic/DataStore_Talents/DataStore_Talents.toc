﻿## Interface: 60000
## Title: DataStore_Talents
## Notes: Stores information about character talents
## Author: Thaoky (EU-Marécages de Zangar)
## Version: 6.0.002
## Dependencies: DataStore
## OptionalDeps: Ace3
## SavedVariables: DataStore_TalentsDB, DataStore_TalentsRefDB
## X-Category: Interface Enhancements
## X-Embeds: Ace3
## X-Curse-Packaged-Version: 6.0.002
## X-Curse-Project-Name: DataStore_Talents
## X-Curse-Project-ID: datastore_talents
## X-Curse-Repository-ID: wow/datastore_talents/mainline

DataStore_Talents.lua
#Data\GlyphToSpellID.lua
Data\ItemIDToGlyphID.lua
Data\GlyphIDToSpellID.lua
