------------------------------------------------------------------------
r8 | thaoky | 2015-01-09 17:35:48 +0000 (Fri, 09 Jan 2015) | 1 line
Changed paths:
   M /trunk/Locales/repoenUS.lua

Added 2 missing localizations
------------------------------------------------------------------------
r7 | thaoky | 2015-01-09 17:11:11 +0000 (Fri, 09 Jan 2015) | 6 lines
Changed paths:
   M /trunk/DataStore_Garrisons.lua
   M /trunk/Options.lua
   M /trunk/Options.xml

- Added an option to select at which level uncollected resources should be reported
- Followers are now stored by id, instead of by name
- Added tracking of abilities, traits, and ability counters
- Fixed resetting the uncollected resource timer
- Fixed usage of options
- XML frames cleanup
------------------------------------------------------------------------
r6 | thaoky | 2014-12-27 13:45:04 +0000 (Sat, 27 Dec 2014) | 1 line
Changed paths:
   M /trunk/DataStore_Garrisons.toc

Updated .toc (forgot options & locales)
------------------------------------------------------------------------
r5 | thaoky | 2014-12-27 13:20:55 +0000 (Sat, 27 Dec 2014) | 1 line
Changed paths:
   A /trunk/.pkgmeta
   M /trunk/DataStore_Garrisons.lua
   A /trunk/Locales
   A /trunk/Locales/deDE.lua
   A /trunk/Locales/enUS.lua
   A /trunk/Locales/esES.lua
   A /trunk/Locales/esMX.lua
   A /trunk/Locales/frFR.lua
   A /trunk/Locales/itIT.lua
   A /trunk/Locales/koKR.lua
   A /trunk/Locales/ptBR.lua
   A /trunk/Locales/repoenUS.lua
   A /trunk/Locales/ruRU.lua
   A /trunk/Locales/zhCN.lua
   A /trunk/Locales/zhTW.lua
   A /trunk/Options.lua
   A /trunk/Options.xml
   A /trunk/locale.xml

Implemented tracking of uncollected resources.
------------------------------------------------------------------------
r3 | thaoky | 2014-12-24 13:58:13 +0000 (Wed, 24 Dec 2014) | 3 lines
Changed paths:
   M /trunk/DataStore_Garrisons.lua
   M /trunk/DataStore_Garrisons.toc

.toc update to use project-version
Added GetNumFollowers()
Minor tweaks
------------------------------------------------------------------------
r2 | thaoky | 2014-12-24 10:17:51 +0000 (Wed, 24 Dec 2014) | 1 line
Changed paths:
   A /trunk/DataStore_Garrisons.lua
   A /trunk/DataStore_Garrisons.toc

Initial commit
------------------------------------------------------------------------
r1 | svn | 2014-12-24 10:10:05 +0000 (Wed, 24 Dec 2014) | 1 line
Changed paths:
   A /branches
   A /tags
   A /trunk

datastore_garrisons/mainline: Initial Import
------------------------------------------------------------------------
