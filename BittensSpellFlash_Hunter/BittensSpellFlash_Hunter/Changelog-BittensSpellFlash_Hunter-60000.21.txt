------------------------------------------------------------------------
r107 | slippycheeze | 2015-01-24 07:07:13 +0000 (Sat, 24 Jan 2015) | 1 line
Changed paths:
   A /tags/60000.21 (from /trunk:105)

update to latest BSF library
------------------------------------------------------------------------
r105 | slippycheeze | 2015-01-17 04:51:09 +0000 (Sat, 17 Jan 2015) | 1 line
Changed paths:
   M /trunk/src/Options.lua
   M /trunk/src/Spells.lua

Flash Cobra and Steady shot at the same time
------------------------------------------------------------------------
r102 | slippycheeze | 2015-01-01 10:30:13 +0000 (Thu, 01 Jan 2015) | 13 lines
Changed paths:
   M /trunk/src/Rotations.lua
   M /trunk/src/Spells.lua

When solo, Mend Pet at 80 percent, not 50 percent

Turns out that the much more conservative approach to using Mend Pet is
sensible in dungeons, but it means that tanking pets end up dying way
more often than they should when faced with level 100+ mobs -- and
non-tanking pets are even worse off.

Using Mend Pet more frequently in this situation means that you are much
less likely to get dangerously low in health and see burst damage take
them out, in turn improving performance.

Since the spell is optional, this also doesn't force a rotation change
on anyone.  (Not that most hunters have pets any longer, but whatever.)
------------------------------------------------------------------------
