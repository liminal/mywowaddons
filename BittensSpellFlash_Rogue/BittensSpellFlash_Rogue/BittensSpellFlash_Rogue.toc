## Interface: 60000
## Version: 60000.10
## Author: SlippyCheeze, Eshria (Idiline) - Defias Brotherhood
## Title: Bitten's SpellFlash: Rogue
## Notes: Replaces Blizzard's default proc highlighting to flash a maximum dps rotation.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-rogue
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Rogue
## X-Curse-Packaged-Version: 60000.10
## X-Curse-Project-Name: Bitten's SpellFlash: Rogue
## X-Curse-Project-ID: bittens-spellflash-rogue
## X-Curse-Repository-ID: wow/bittens-spellflash-rogue/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\load.xml
