# My WoW Addons

Since none of the automatic addon-trackers work with linux I decided to
track mine via git. I do have a script that automatically links them into
my Addon directory but that's not included here. I'm also working on a java
app to manage the addons as well.
