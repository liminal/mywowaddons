## Interface: 60000
## Version: 60000.7
## Author: SlippyCheeze, Xemnosyst
## Title: Bitten's SpellFlash: Monk
## Notes: Replaces Blizzard's default proc highlighting to flash 1) a maximum dps rotation, 2) a maximum survivability tanking rotation, or 3) more informative healing proc highlighting.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-monk
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Monk
## X-Curse-Packaged-Version: 60000.7
## X-Curse-Project-Name: Bitten's SpellFlash: Monk
## X-Curse-Project-ID: bittens-spellflash-monk
## X-Curse-Repository-ID: wow/bittens-spellflash-monk/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
