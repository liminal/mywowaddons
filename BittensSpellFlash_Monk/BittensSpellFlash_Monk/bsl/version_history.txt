== Version 60000.5
Initialize estimated target counts to try and fix some odd "number compared
to nil" errors that people have encountered.

== Version 60000.4
* Ignore multistrike hits when calculating AoE target estimates.

These come with the same timestamp as the main event, but as distinct log
entries, so they were fooling the calculation into thinking there were many
more targets than really existed.  Thanks, Shamans, for finding this.

== Version 60000.3.3
* Protect c.GetChargeInfo against unknown spells.

To avoid bugs, if you do not know the spell we return 0 charges, 9001 seconds
to next charge, and 9001 seconds to cap.  These seems like the safest return
values, and avoid needing to guard every invocation with a check the spell is
already known.

== Version 60000.3.2
* Added PreFlash to individual rotations.

This is a pretty internal change, but allows a PreFlash function to be defined
on any rotation, which will be run prior to any other flashing being done.
This exactly mirrors the PreFlash function on the class module, but on a
per-rotation basis.

This is to support the Warlock module, which has per-class resources to track,
but is applicable to any rotation that has unique management to do prior to
rotation calculations.

== Version 60000.3.1
* Fix range detection (c.DistanceAtThe{Least,Most}) for 6.0 API changes.
* Minor cleanups elsewhere.
* Added "Armor" food type placeholder, against the future.

== Version 60000.3.0
* New maintainer: http://www.curse.com/users/slippycheeze
  - HUGE thanks to Xemnosyst for all the work done to get this
    to where it is today, and keep it awesome.  I only hope to
    be half as good on my best days.

* Update to utf-8-unix encoding everywhere.
* Standardize on 3 spaces for indentation.
* Added AddonLoader support to toc. (delay loading only)
* Updated author details.

== Version 60000.2.1
* AoE target count, via c.EstimatedHarmTargets and c.EstimatedHealTargets.

== Version 60000.2.0
* Updated buffs and foods for 6.0 (and WoD).

== Version 60000.1.2
* Bugfix for occasional Lua error.

== Version 60000.1.1
* Bugfix: Getting spell cast time changed w/ patch 6.0.

== Version 60000.1.0
* Making the "Out of Date" status go away.

== Version 50400.4.2
This version only affectly Feral Druids.
* Small tweak for flashing forms in c.DelayPriorityFlash.

== Version 50400.4.1
* Added flashing for WoW's 9th Anniversary Celebration Package.

== Version 50400.4.0
This version is required for the latest Death Kight release.
* Added c.Flashable().

== Version 50400.3.0
This version only adds convenience flashing - it is not a required update for any class.
* Added flashing for some new foods and items from the Timeless Isle.

== Version 50400.2.0
This version supports the latest Hunter release.
* c.Flashing{} now contains an entry for every spell that is flashing.
* c.IsTanking() now accepts a "unit" argument.
* Removed c.CheckFirstForTaunts().

== Version 50400.1.7
Updating to this version is not necessary.  It causes flashes for the more new food buffs.

== Version 50400.1.6
Updating to this version is not necessary.  It causes flashes for the new Noodle food buffs.

== Version 50400.1.5
Among other things, this is a bugfix release.  Everyone should update.
* c.GetChargeInfo() now takes into account whether the spell is currently casting.
* The CastSucceeded_FromLog callback now passes more arguments.
* Bugfix - c.HasDebuff(), c.HasMyDebuff(), and c.GetPowerPercent() had problems.

== Version 50400.1.4
This version supports the latest Shaman release.
* Added c.GetPowerPercent()

== Version 50400.1.3
This version supports the latest Rogue release.
* Added a new event callback for rotations: CastSucceeded_FromLog

== Version 50400.1.2
This version supports the latest Pally release.
* c.DelayPriorityFlash() returns a second value: true if the flashing spell is only a predictor.

== Version 50400.1.1
This version breaks backward compatibility for the Druid, Hunter, and Monk modules.
* Added c.GetChargeInfo().
* c.RegisterForFullChannels() accepts a new argument indicating that the channel is indicated by a buff, rather than the normal channeling mechanism (read: Spinning Crane Kick)
* Slight improvements to c.GetBusyTime() and c.GetPower().
* The Cost entry of casting info tables is now itself a table from power type => cost.

== Version 50400.1.0
* Slightly smarter determination of Target GUID for the CastStarted, CastFailed and CastSucceeded callbacks.
* All the functions that tested for a buff or its duration now takes a list of spells that apply it, rather than just one.
* c.GetPower() now uses the regen of your primary power type if not specified.
* c.GetQueuedInfo() now favors spells that are on the GCD.

== Version 4.13.7
This version supports the latest releases for tanking classes.
* Added c.COMMON_TANKING_BUFFS.
* c.FlashMitigationBuffs will now match the spell id of buffs when checking if one is already up, rather than the name.

== Version 4.13.6
WARNING: If you install no nolib versions of my addons, you will have to download Bitten's Utils now.  If you don't know what I'm talking about, you can ignore this, and even deleted Bitten's SpellFlash Library, since it comes packaged with your class module(s) anyway.
* Splitting some non-SpellFlash-specific functions off into a different library: Bitten's Utils, for use with other addons.

== Version 4.13.5
This version supports the latest Druid release.
* Bugfix: c.DelayPriorityFlash() was not respecting the AoEColor flag.

== Version 4.13.4
This version fixes an issue for Fire Mages who use Presence of Mind.  There may be other classes/specs which had the same bug, but I'm not sure.
* Bugfix: no longer consider an aura pending if it was reported to be applied before the spell was reported to be cast.

== Version 4.13.3
This version supports the latest Warrior release.
* Added a callback for rotations: Avoided().

== Version 4.13.2
* Oops - forgot something in 4.13.1.
