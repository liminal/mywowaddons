== Version 60000.14
Fix flashing for Unholy pet summorn.

== Version 60000.13
Update to latest BSF library.

== Version 60000.12
Fix Soul Reaper.  It would never fire, which is crazy.  One tiny line later
and it should now work, which should see a nice jump in DK DPS across
all specs.

== Version 60000.11
Debugging and work-around for ticket 200, in which our rune cooldown is nil
for a level 58 DK.  Since I can't reproduce this trivially, and I don't want
to try and level to 58 to verify it for sure, this adds inline debugging and
protects the code against the error spam.

== Version 60000.10
Rewrote Unholy rotation for 6.0; this brings it up to both technical and
in-game parity with frost and blood.  As with any major rewrite bugs may
exist, but this is fairly well play tested, and should be solid.

== Version 60000.9
Bump priority of plague strike for DW Frost, so that we actually get blood
plague on, well, pretty much anything.  It was spending almost exclusively on
Obliterate instead, just because of the way runes stacked up in the rotation.

== Version 60000.8
* Fix manual BT error for all specs.

== Version 60000.7
* Fix Unholy lua error, from operator precedence fail.

== Version 60000.6
* Bump priority of Plague Strike for Blood Plague.
* Various internal cleanups to rotation.
* Cleaner "delay until just before this spell" implementation.
  - This should improve prediction and flash timing for resource
    cap avoidance.
* Flash Death Strike with Dark Succor for self-healing as Frost.
* Fixed Death Strike inappropriately flashing for Blood.
  - possibly other inappropriate flashing for other specs, if
    an ability required more than one rune.

== Version 60000.5
* Internal rotation changes to smooth out flashing, etc.

This shouldn't substantially change the externally visible rotation, but it
does bring a number of improvements -- notably, that spells should now
indicate if they expect to be ready *now*, or if they are flashing to indicate
what the next action will be once a rune cooldown is finished.

Hopefully that will both improve the experience, and help highlight anywhere
that our logic for handling runes and/or Runic Power is off.

== Version 60000.4
* Don't pool RP toward the end of a fight, use it instead.
* Soul Reaper should not fire at very low target health.
* ERW should not flash unless all runes are used up.

This release focuses on tuning fights against both quest-level mobs, and
bosses, with a view to avoiding wasting resources by pooling too long, or by
using SR when it is very unlikely to actually land.

== Version 60000.3.1
* all: fix misuse of blood boil on targets without both diseases.

== Version 60000.3.0
* Frost AoE rotation enabled (with newer BSFL.)
* Defile added to rotations.
* Convert to utf-8-unix.
* Reindent to modern lua 3-space indentation everywhere.

== Version 60000.1.1
* fix Frost Strike with Killing Machine for DW Frost.

== Version 60000.1.0
* Updated for 6.0.2, major rewrites to all rotations.
* NOTE: currently no AoE or "solo" DPS rotations exist.
* Added "Macroed BT" option (assumes *all* abilities have it macroed).
* Removed "Mastersimple", as it is thoroughly dead with 6.0 changes.

== Version 50400.4.0
* Frost: Added an option to switch to a Mastersimple rotation.  See the website for (a few) more details.

== Version 50400.3.2
* Blood: Adjusted some flashing for Rune Strike when you take Runic Corruption, to waste less Runic Power in certain situations.
* Blood: Death and Decay flashes when in AoE mode, it is free, and your diseases have plenty of time left.
* Blood: Bugfix - Smoothed out a quirk having to do with the Runic Power granted by Blood Boil.
* Blood: Bugfix - one of the Rune Strike priorities was firing at the wrong time.

== Version 50400.3.0
* Frost: Updated the dual-wield rotation (because there is much more haste on gear now).

== Version 50400.2.2
* All: Flashes WoW's 9th Anniversary Celebration Package.

== Version 50400.2.1
* Blood: Bugfix - Soul Reaper and Heart Strike were sometimes flashing when you had no target.

== Version 50400.2.0
* Blood: Added Soul Reaper.

== Version 50400.1.2
* Blood: Adjusted the priority and conditions for Dancing Rune Weapon when you have 4pT16.
* Blood: Does not flash Vampiric Blood when solo until you have runes to cast Death Strike.
* All: Fixed quirky flashing as you cast Empower Rune Weapon.

== Version 50400.1.1
* Blood: Lowered the priority of Dancing Rune Weapon.  Since it no longer requires Runic Power, it is no longer necessary take advantage of situations when you have it.  It is still top priority in damage mode.
* Frost: Added Horn of Winter to the AoE rotation.

== Version 50400.1.0
* Blood: Updated Dancing Rune Weapon for 5.4.
* Blood: Bugfix - it was assuming that Blood Boil cast under Crimson Scourge would grant 10 Runic Power, but it does not.
* All: Accounts for the new Glyph of the Loud Horn.
* All: Updated the conditions for Plague Leech for 5.4.
* All: Added support (but not translations) for Italian localization.  To contribute translations go to http://wow.curseforge.com/addons/bittens-spellflash-dk/localization/.
* All: Smoothed out some quirky flashing.
* All: More accurate anticipation of which runes will be used by queued spells.

== Version 3.7.1
* Blood: Now checks for the proc from Indomitable Primal Diamond before flashing your mitigation cooldowns.
* Blood: Now checks for a number of external mitigation cooldowns before flashing one of your own.

== Version 3.7.0
* Blood: Added flashing for Plague Leech.
* Blood: Higher-priority disease applications are now based on Weakened Blows, rather than Blood Plague.
* Blood: Don't flash Bone Shield out of combat until an enemy is detected.
* Blood: Bugfix - No longer expect Scarlet Fever to trigger when Blood Boil misses.
* Blood: Bugfix - Blood Boil was sometimes flashing at its higher priority when it should have been at the lower.
* All: Anticipate incoming Blood Charges.

== Version 3.6.6
* Frost: Ok, maybe this time I will have fixed all the Lua errors ...

== Version 3.6.5
* All: Bugfix for a Lua error.

== Version 3.6.4
* Unholy: Bugfix - one minor case had less-than-smooth flashing on non-English clients.
* All: Now flashes food buffs.

== Version 3.6.3
Update for patch 5.3.
* Blood: Bone Shield now refreshes a 1 minute when out of combat (instead of 2 minutes).

== Version 3.6.2
* Unholy - Bugfix for a Lua error.

== Version 3.6.1
* Bugfix - there were a couple more places to check for 45% vs 35% health when wearing 4pT15.
* Death Siphon, Death Strike, Death Pact, Rune Tap and Conversion now all consider incoming heals in their logic. 

== Version 3.6.0
* Blood: Blood Boil was flashing too much in the single-target solo rotation (a bug introduced in a recent version).
* Frost & Unholy: Soul Reaper now flashes at 45% if you are wearing 4pT15.
* All: Now flashes Strangulate & Asphyxiate as interrupts.
* All: Horn of Winter will now always flash if a raid member needs the buff.

== Version 3.5.2
* Frost: Bugfix for a Lua error.

== Version 3.5.1
NOTE: This version includes a new version of Bitten's SpellFlash Library that I tried to test well, but if you find any issues please let me know right away.
* Unholy: No longer tries to flash Icy Touch for Frost Fever (now simply uses Plague Strike).

== Version 3.5.0
NOTE: Your options will be reset when you upgrade to this version.
* Repackaging so that Bitten's SpellFlash Library is included with the addon, instead of downloaded separately.
* Several other internal changes.
* Blood: Major work to the solo rotation, including AoE.
* Blood: Solo mode can now be disabled in the options.
* Frost: Added an AoE rotation.
