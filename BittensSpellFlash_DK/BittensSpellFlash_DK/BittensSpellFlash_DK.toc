## Interface: 60000
## Version: 60000.20
## Author: Xemnosyst, SlippyCheeze
## Title: Bitten's SpellFlash: DK
## Notes: Replaces Blizzard's default proc highlighting to flash 1) a maximum dps rotation or 2) a maximum survivability tanking rotation.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-dk
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Death Knight
## X-Curse-Packaged-Version: 60000.20
## X-Curse-Project-Name: Bitten's SpellFlash: DK
## X-Curse-Project-ID: bittens-spellflash-dk
## X-Curse-Repository-ID: wow/bittens-spellflash-dk/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
