﻿## Interface: 60000
## Version: 1.0.7
## X-AddonType: FULL
## Title: Links in Chat
## Title-deDE: Links in Chat
## Title-esES: Links in Chat
## Title-esMX: Links in Chat
## Title-frFR: Links in Chat
## Title-itIT: Links in Chat
## Title-koKR: Links in Chat
## Title-ptBT: Links in Chat
## Title-ruRU: Links in Chat
## Title-zhTW: Links in Chat
## Notes: Shows a popup window when clicking web-links in chat.
## Notes-deDE: Zeigt ein popup-fenster, beim klicken auf chat-links.
## Notes-esES: Shows a popup window when clicking web-links in chat.
## Notes-esMX: Shows a popup window when clicking web-links in chat.
## Notes-frFR: Fais apparaitre une fenetre quand l'utilisateur clique sur un lien.
## Notes-itIT: Mostra una finestra popup quando si clicca un collegamento nella chat.
## Notes-koKR: Shows a popup window when clicking web-links in chat.
## Notes-ptBT: Shows a popup window when clicking web-links in chat.
## Notes-ruRU: Shows a popup window when clicking web-links in chat.
## Notes-zhTW: Shows a popup window when clicking web-links in chat.
## Dependencies:
## SavedVariables: LINKSINCHAT_settings
## LoadOnDemand: 0
## DefaultState: enabled

UIDropDownNOTAINTMenu.xml

Locale.lua
SearchProvider.lua
HiddenTooltip.lua

LinkWebParsing.lua
LinksInChat.lua
LinksInChat.xml