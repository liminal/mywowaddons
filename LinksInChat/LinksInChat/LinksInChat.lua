﻿--####################################################################################
--####################################################################################
--Main class
--####################################################################################
--####################################################################################
--Dependencies: LinkWebParsing.lua, Locale.lua, SearchProvider.lua

--LINKSINCHAT_GameVersion		= nil; --UIversion number of the game for quick lookup for other parts of the code for compatibility
	--if (LINKSINCHAT_GameVersion < 50300) then
		--Before patch 5.3.0

--local LINKSINCHAT_addon_version = GetAddOnMetadata("LinksInChat", "Version");	--Version number for the addon

--####################################################################################
--####################################################################################
--Event Handling and Cache
--####################################################################################

LinksInChat			= {};	--Global declaration
LinksInChat.__index	= LinksInChat;

local LinkWebParsing	= LinksInChat_LinkWebParsing;		--Local pointer
local Locale			= LinksInChat_Locale;
local SearchProvider	= LinksInChat_SearchProvider;

LINKSINCHAT_settings	= {};		--Array of Settings	(SAVED TO DISK)
--[[
	Color				:: Number	: Hex formatted color for links (RRGGBB)
	AutoHide			:: Number	: Hide copy frame after N seconds. Positive number in seconds or -1 to disable the feature.
	IgnoreHyperlinks 	:: Boolean	: Ignore any hyperlinks [myitem] etc. and will just work for web links www. and http://
	Extra				:: Boolean	: Enable Alt-clicking in character frame, auction house, black market and achievement frame.
	Simple				:: Boolean	: Always simple-search for providers (only search for item name and dont try to lookup using spellid, factionname etc).
	UseHTTPS			:: Boolean	: Use https:// or http:// (true).
	AlwaysEnglish		:: Boolean  : Always use english search provider.
	Provider			:: String	: Dropdown of several different search providers for itemlinks (Google, Bing, Wowdb, Wowhead, etc).
]]--

--Local variables that cache stuff so we dont have to recreate large objects
local cache_Color_HyperLinks	= "FF0080"; --Pink hyperlink color
local cache_Provider			= nil; --Current selected provider
local hook_ChatFrame_OnHyperlinkShow		= nil; --ChatFrame_OnHyperlinkShow; --Original function
local hook_QuestMapLogTitleButton_OnClick	= nil; --QuestMapLogTitleButton_OnClick; --Original function
local hook_ACHIEVEMENT_TRACKER_MODULE		= nil; --Blizzard_AchievementObjectiveTracker.lua


--Handles the events for the addon
function LinksInChat:OnEvent(s, event, ...)
	if (event == "PLAYER_ENTERING_WORLD") then
		--Startup

		--Register for chat channels (rest is done in LinkWebParsing)
		LinkWebParsing:RegisterMessageEventFilters(true);
		---------------------------------------------------------------------------

		--Apply default settings.
		cache_Color_HyperLinks	= self:GetCurrentSetting("Color") or "FF0080"; --Pink color
		local s_AutoHide		= self:GetCurrentSetting("AutoHide") or -1;
		local s_Ignore			= self:GetCurrentSetting("IgnoreHyperlinks") or false;
		local s_Extra			= self:GetCurrentSetting("Extra") or true;
		local s_Simple			= self:GetCurrentSetting("Simple") or false;
		local s_HTTPS			= self:GetCurrentSetting("UseHTTPS") or true;
		local s_English			= self:GetCurrentSetting("AlwaysEnglish") or false;
		local s_Provider		= self:GetCurrentSetting("Provider") or "wowhead";

		--Validate Provider
		SearchProvider:InitializeProvider(s_English);
		if (SearchProvider:ProviderExists(s_Provider) ~= true) then s_Provider = "wowhead"; end
		cache_Provider = SearchProvider:GetProvider(s_Provider);

		self:SetCurrentSetting("Color", cache_Color_HyperLinks);
		self:SetCurrentSetting("AutoHide", s_AutoHide);
		self:SetCurrentSetting("IgnoreHyperlinks", s_Ignore);
		self:SetCurrentSetting("Extra", s_Extra);
		self:SetCurrentSetting("Simple", s_Simple);
		self:SetCurrentSetting("UseHTTPS", s_HTTPS);
		self:SetCurrentSetting("AlwaysEnglish", s_English);
		self:SetCurrentSetting("Provider", s_Provider);
		---------------------------------------------------------------------------

		--Hook into chat
		hook_ChatFrame_OnHyperlinkShow = ChatFrame_OnHyperlinkShow; --Save pointer to Original function
		ChatFrame_OnHyperlinkShow = LinksInChat_ChatFrame_OnHyperlinkShow; --Override with our own function.
		--hooksecurefunc("ChatFrame_OnHyperlinkShow", LinksInChat_ChatFrame_OnHyperlinkShow)
		---------------------------------------------------------------------------

		--Hook into questlog
		hook_QuestMapLogTitleButton_OnClick = QuestMapLogTitleButton_OnClick; --Save pointer to Original function
		QuestMapLogTitleButton_OnClick = LinksInChat_QuestMapLogTitleButton_OnClick; --Override with our own function.
		---------------------------------------------------------------------------

		--Hook into objective tracker
		hook_ACHIEVEMENT_TRACKER_MODULE = ACHIEVEMENT_TRACKER_MODULE["OnBlockHeaderClick"];
		ACHIEVEMENT_TRACKER_MODULE["OnBlockHeaderClick"] = LinksInChat_ACHIEVEMENT_TRACKER_MODULE;
		--Using a post-hook on the quest details frame
		hooksecurefunc("QuestLogPopupDetailFrame_Show", LinksInChat_QuestLogPopupDetailFrame_Show);
		---------------------------------------------------------------------------

		--Hook into Character frame
		CharacterHeadSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterNeckSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterShoulderSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterBackSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterChestSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterShirtSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterTabardSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterWristSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterHandsSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterWaistSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterLegsSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterFeetSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterFinger0Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterFinger1Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterTrinket0Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterTrinket1Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterMainHandSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		CharacterSecondaryHandSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);

		--Hook into Default lootframe
		--Source: LootFrame.lua and .xml
		local CONST1, i = "LootButton", 1; --LootButton%
		local objItem = _G[CONST1..i];

		while (objItem ~= nil) do
			objItem:HookScript("OnClick", LinksInChat_LootButton_OnClick);
			i = i +1; --Increment counter
			objItem = _G[CONST1..i];
		end--while
		---------------------------------------------------------------------------

		--Hook into Default grouploot frames
		--Source: LootFrame.lua and .xml
		local CONST1, CONST2, i = "GroupLootFrame", "IconFrame", 1; --GroupLootFrame%.IconFrame
		local objName1, objName2 = _G[CONST1..i], nil;	--Frame with a icon as subframe
		if (objName1 ~= nil) then objName2 = objName1[CONST2]; end

		while (objName1 ~= nil) do
			objName2:HookScript("OnClick", LinksInChat_GroupLootFrame_OnClick); --This is the button that holds the icon in the grouploot frame
			i = i +1; --Increment counter
			objName1 = _G[CONST1..i];
			if (objName1 ~= nil) then objName2 = objName1[CONST2]; end
		end--while
		---------------------------------------------------------------------------


		--Hook into Default bags
		--Source: ContainerFrame.lua and .xml  Lists 13 ContainerFrames each with 36 items in the xml. However only the 5 first frames are used by the bags. Rest must be bank and guildbank?
		local CONST1, CONST2, i, j = "ContainerFrame", "Item", 1, 1; --ContainerFrame%Item%
		local objFrame, objItem = _G[CONST1..i], _G[CONST1..i..CONST2..j];

		while (objFrame ~= nil) do
			while (objItem ~= nil) do
				objItem:HookScript("OnClick", LinksInChat_ContainerFrameItem_OnClick);
				j = j +1; --Increment counter
				objItem = _G[CONST1..i..CONST2..j]; --ContainerFrame%Item%
			end--while objItem

			i = i +1;	--Increment counter
			j = 1;		--Reset before next inner loop
			objFrame = _G[CONST1..i];
			objItem = _G[CONST1..i..CONST2..j]; --ContainerFrame%Item%
		end--while objFrame
		---------------------------------------------------------------------------

		--Hook into Default bank
		local CONST1, i = "BankFrameItem", 1; --BankFrameItem%
		local objItem = _G[CONST1..i];

		while (objItem ~= nil) do
			objItem:HookScript("OnClick", LinksInChat_ContainerFrameItem_OnClick); --Reuse the same function as for bags
			i = i +1;	--Increment counter
			objItem = _G[CONST1..i]; --BankFrameItem%
		end--while
		---------------------------------------------------------------------------

		--Hook into Default merchantframe
		--Source: MerchantFrame.lua and .xml
		local CONST1, CONST2, i = "MerchantItem", "ItemButton", 1; --MerchantItem%ItemButton
		local objItem = _G[CONST1..i..CONST2];

		while (objItem ~= nil) do
			objItem:HookScript("OnClick", LinksInChat_MerchantItemButton_OnClick);
			i = i +1;	--Increment counter
			objItem = _G[CONST1..i..CONST2]; --MerchantItem%ItemButton
		end--while
		---------------------------------------------------------------------------

		--Hook into the 'Bagnon' addon's bagframes.
		if (IsAddOnLoaded("Bagnon") == true) then
			--The Bagnon bag buttons are created when the user first opens the bags. No event is reliable for when this is done so we use a timer.
			local callback = self["CALLBACK_Bagnon"]; --If its nil then its already been loaded
			if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.
		end
		---------------------------------------------------------------------------

		s:RegisterEvent("ADDON_LOADED"); --Inspect frame, Auction house, Black market, Achivement and other addons are loaded on demand.
		s:RegisterEvent("BANKFRAME_OPENED"); --When the bank frame is opened
		---------------------------------------------------------------------------

		--After the first call to RegisterMessageEventFilters we dont need this anymore
		s:UnregisterEvent("PLAYER_ENTERING_WORLD");

		--[[
		local h = "\124cffffff00\124Hachievement:8304:"..UnitGUID("player")..":0:0:0:0:0:0:0:0\124h[Mount Parade]\124h\124r";
		local h1 = "\124cffe5cc80\124Hitem:104406:0:0:0:0:0:0:0:0:0:0\124h[Hellscream's War Staff]\124h\124r";
		local h2 = "\124cffffd000\124Hspell:108978\124h[Alter Time]\124h\124r";
		local h3 = "\124cffffffff\124Hitem:89112:0:0:0:0:0:0:0:0:0:0\124h[Mote of Harmony]\124h\124r";
		local h4 = "\124cffffd000\124Hspell:44457\124h[Living Bomb]\124h\124r";

		local h5 = "\124cffa335ee\124Hitem:112920:0:0:0:0:0:0:0:0:491:0:0:0\124h[Korven's Crimson Crescent]\124h\124r";
		local h6 = "\124cffa335ee\124Hitem:112920:0:0:0:0:0:0:0:0:491:0:2:449:448\124h[Korven's Crimson Crescent]\124h\124r"; --10th digit and beyond
		local h7 = "\124cffa335ee\124Hitem:112920:0:0:0:0:0:0:0:0:491:0:2:450:171\124h[Korven's Crimson Crescent]\124h\124r"; --10th digit and beyond
		--itemID:enchant:gem1:gem2:gem3:gem4:suffixID:uniqueID:level:reforgeId:upgradeId
		--itemID:enchant:gem1:gem2:gem3:gem4:suffixID:uniqueID:level:upgradeId:instanceDifficultyID:numBonusIDs:bonusID1:bonusID2...
		print("  ");
		print("        "..h);
		print("        "..h1);
		print("        "..h2);
		print("        "..h3);
		print("        "..h4);
		print("        "..h5);
		print("        "..h6);
		print("        "..h7);
		print("  ");
		--local s6 = LinkWebParsing:split(h6, ":"); for i=1, #s6 do	print("   "..tostring(s6[i])); end
		--]]--
	elseif(event=="BANKFRAME_OPENED") then
		--Hook into Default bank reagent frame (buttons are created when the user clicks the reagent-tab)
		local callback = self["CALLBACK_ReagentBankFrameItem"];
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the event is triggered
		--s:UnregisterEvent("BANKFRAME_OPENED"); --Is never unregistered because user might not click the reagent tab the first time.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_GuildBankUI") then
		local callback = self["CALLBACK_Blizzard_GuildBankUI"];
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_VoidStorageUI") then
		local callback = self["CALLBACK_Blizzard_VoidStorageUI"]; --VoidStorageStorageButton1
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_InspectUI") then
		--The addon is loaded the first time the player uses /inspect
		InspectHeadSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectNeckSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectShoulderSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectBackSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectChestSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectShirtSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectTabardSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectWristSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectHandsSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectWaistSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectLegsSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectFeetSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectFinger0Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectFinger1Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectTrinket0Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectTrinket1Slot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectMainHandSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);
		InspectSecondaryHandSlot:HookScript("OnClick", LinksInChat_CharacterPaperDoll_OnClick);

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_AchievementUI") then
		local callback = self["CALLBACK_Blizzard_AchievementUI"]; --If its nil then its already been loaded
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_AuctionUI") then
		--We need to make several hooks for the Auction House UI. One for each line in the UI, plus one for the line and one for the button with the item icon in it
		if (IsAddOnLoaded("Auc-Advanced") == true) then self["CALLBACK_Blizzard_AuctionUI"] = nil; end --If the addon 'Actioneer' is enabled then we will not hook into the AH.
		local callback = self["CALLBACK_Blizzard_AuctionUI"];
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_BlackMarketUI") then
		--The Black Market UI and buttons are created a little after the Blizzard_BlackMarketUI is loaded. No event is reliable for when this is done so we use a timer.
		local callback = self["CALLBACK_Blizzard_BlackMarketUI"];
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_PetJournal") then
		--We need to make several hooks for the ToyBoy UI. One for each button in the UI.
		local callback = self["CALLBACK_Blizzard_PetJournal"];
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_EncounterJournal") then
		--We need to make several hooks for the Dungeon Journal. One for each button in the loot list.
		local callback = self["CALLBACK_Blizzard_EncounterJournal"];
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Blizzard_TradeSkillUI") then
		--We need to make several hooks for the Tradeskill UI. One for each reagent in the UI.
		local callback = self["CALLBACK_Blizzard_TradeSkillUI"];
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Bagnon") then
		--The Bagnon bag buttons are created when the user first opens the bags. No event is reliable for when this is done so we use a timer.
		local callback = self["CALLBACK_Bagnon"]; --If its nil then its already been loaded
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Bagnon_GuildBank") then
		--The Bagnon guildbank bag buttons are created when the user first opens the guildbank.
		local callback = self["CALLBACK_BagnonGuildBank"]; --If its nil then its already been loaded
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	elseif(event=="ADDON_LOADED" and tostring(select(1,...)) == "Bagnon_VoidStorage") then
		--The Bagnon voidstorage bag buttons are created when the user first opens the void storeage.
		local callback = self["CALLBACK_BagnonVoidStorage"]; --If its nil then its already been loaded
		if (callback ~= nil) then C_Timer.After(1, callback); end --Trigger function 1 seconds after the addon is loaded.

	end
	return nil;
end


--####################################################################################
--####################################################################################
--Callback
--####################################################################################


function LinksInChat:CALLBACK_ReagentBankFrameItem()
	--Reagent frame buttons does not exist until the user presses the reagent button in the bank. Starts a sleep loop when the bank frame is opened until it closes.
	local CONST1, i = "ReagentBankFrameItem", 1;
	local objItem = _G[CONST1..i];	--ReagentBankFrameItem%

	if (objItem == nil) then
		if (BankFrame:IsShown() == false) then return nil; end --Will make the loop stop if the BankFrame is closed. Will start again whenever the bank frame is openend again.
		local callback = LinksInChat["CALLBACK_ReagentBankFrameItem"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objItem ~= nil) do
		objItem:HookScript("OnClick", LinksInChat_ContainerFrameItem_OnClick); --Reuse the same function as for bags
		i = i +1; --Increment counter
		objItem = _G[CONST1..i];
	end--while

	LinksInChat["CALLBACK_ReagentBankFrameItem"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_GuildBankUI()
	--We need to make several hooks for the GuildBank UI. One for each line in the UI, plus one for the line and one for the button with the item icon in it
	local CONST1, CONST2, i, j = "GuildBankColumn", "Button", 1, 1; --GuildBankColumn%Button%
	local objFrame, objItem = _G[CONST1..i], _G[CONST1..i..CONST2..j];

	if (objItem == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_GuildBankUI"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objFrame ~= nil) do
		while (objItem ~= nil) do
			objItem:HookScript("OnClick", LinksInChat_GuildBankItem_OnClick);
			j = j +1; --Increment counter
			objItem = _G[CONST1..i..CONST2..j]; --GuildBankColumn%Button%
		end--while objItem

		i = i +1;	--Increment counter
		j = 1;		--Reset before next inner loop
		objFrame = _G[CONST1..i];
		objItem = _G[CONST1..i..CONST2..j]; --GuildBankColumn%Button%
	end--while objFrame

	LinksInChat["CALLBACK_Blizzard_GuildBankUI"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_VoidStorageUI()
	--We need to make several hooks for the VoidStorage UI.
	local CONST1, i = "VoidStorageStorageButton", 1; --VoidStorageStorageButton%
	local objItem = _G[CONST1..i];

	if (objItem == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_VoidStorageUI"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objItem ~= nil) do
		objItem:HookScript("OnClick", LinksInChat_VoidStorageItemButton_OnClick);
		i = i +1; --Increment counter
		objItem = _G[CONST1..i]; --VoidStorageStorageButton%
	end--while objItem


	local CONST1, i = "VoidStorageDepositButton", 1; --VoidStorageDepositButton%
	local objItem = _G[CONST1..i];
	while (objItem ~= nil) do
		objItem:HookScript("OnClick", LinksInChat_VoidStorageItemButton_OnClick);
		i = i +1; --Increment counter
		objItem = _G[CONST1..i]; --VoidStorageDepositButton%
	end--while objItem

	local CONST1, i = "VoidStorageWithdrawButton", 1; --VoidStorageWithdrawButton%
	local objItem = _G[CONST1..i];
	while (objItem ~= nil) do
		objItem:HookScript("OnClick", LinksInChat_VoidStorageItemButton_OnClick);
		i = i +1; --Increment counter
		objItem = _G[CONST1..i]; --VoidStorageWithdrawButton%
	end--while objItem

	LinksInChat["CALLBACK_Blizzard_VoidStorageUI"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_AchievementUI()
	--We need to make several hooks for the Achievement UI. One for each line in the UI
	local CONST1, i = "AchievementFrameAchievementsContainerButton", 1;
	local objName1 = _G[CONST1..i] --AchievementFrameAchievementsContainerButton%

	if (objName1 == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_AchievementUI"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_AchievementButton_OnClick);
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
	end--while

	LinksInChat["CALLBACK_Blizzard_AchievementUI"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_AuctionUI()
	--We need to make several hooks for the Auction House UI. One for each line in the UI, plus one for the line and one for the button with the item icon in it
	local CONST1, CONST2, i = "BrowseButton", "Item", 1;
	local objName1, objName2 = _G[CONST1..i], _G[CONST1..i..CONST2]; --BrowseButton%, BrowseButton%Item

	if (objName1 == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_AuctionUI"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_BrowseButtonTemplate_OnClick); --Hook for the most of the Auction House Line
		objName2:HookScript("OnClick", LinksInChat_BrowseButtonTemplateItem_OnClick); --This is the button that holds the icon
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
		objName2 = _G[CONST1..i..CONST2];
	end--while

	LinksInChat["CALLBACK_Blizzard_AuctionUI"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_BlackMarketUI()
	--We need to make several hooks for the Black Market UI. One for each line in the UI, plus one for the line and one for the button with the item icon in it
	--Source: Blizzard_BlackMarketUI.xml
	local CONST1, i = "BlackMarketScrollFrameButton", 1;
	local objName1 = _G[CONST1..i];	--BlackMarketScrollFrameButton%
	local objName2 = nil;			--BlackMarketScrollFrameButton%.Item
	if (objName1 ~= nil) then objName2 = objName1["Item"]; end

	if (objName1 == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_BlackMarketUI"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_BlackMarketItemTemplate_OnClick); --Hook for the most of the Auction House Line
		objName2:HookScript("OnClick", LinksInChat_BlackMarketItemTemplateItem_OnClick); --This is the button that holds the icon
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
		if (objName1 ~= nil) then objName2 = objName1["Item"]; end
	end--while

	LinksInChat["CALLBACK_Blizzard_BlackMarketUI"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_PetJournal()
	--We need to make several hooks for the ToyBoy UI. One for each button in the UI.
	local CONST1, i = "ToySpellButton", 1;
	local objName1 = _G[CONST1..i]; --ToySpellButton%

	if (objName1 == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_PetJournal"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_ToySpellButton_OnClick);
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
	end--while

	--We need to make several hooks for the PetJournal UI. One for each button in the UI + for the icon button.
	local CONST1, CONST2, i = "PetJournalListScrollFrameButton", "dragButton", 1;
	local objName1 = _G[CONST1..i];	--PetJournalListScrollFrameButton%
	local objName2 = nil;			--subelement of PetJournalListScrollFrameButton.dragButton
	if (objName1 ~= nil) then objName2 = objName1[CONST2]; end

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_PetJournalListScrollFrameButton_OnClick);
		objName2:HookScript("OnClick", LinksInChat_PetJournalListScrollFrameButton_dragButton_OnClick); --This is the button that holds the icon
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
		if (objName1 ~= nil) then objName2 = objName1[CONST2]; end
	end--while

	--We need to make several hooks for the MountJournal UI. One for each button in the UI + for the icon button.
	local CONST1, CONST2, i = "MountJournalListScrollFrameButton", "DragButton", 1; --The 'd' in dragbutton is capitalized in the mountjurnal compared to the petjournal
	local objName1 = _G[CONST1..i];	--PetJournalListScrollFrameButton%
	local objName2 = nil;			--subelement of PetJournalListScrollFrameButton.DragButton
	if (objName1 ~= nil) then objName2 = objName1[CONST2]; end

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_MountJournalListScrollFrameButton_OnClick);
		objName2:HookScript("OnClick", LinksInChat_MountJournalListScrollFrameButton_DragButton_OnClick); --This is the button that holds the icon
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
		if (objName1 ~= nil) then objName2 = objName1[CONST2]; end
	end--while

	LinksInChat["CALLBACK_Blizzard_PetJournal"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_EncounterJournal()
	--We need to make several hooks for the ToyBoy UI. One for each button in the UI.
	local CONST1, i = "EncounterJournalEncounterFrameInfoLootScrollFrameButton", 1;
	local objName1 = _G[CONST1..i]; --EncounterJournalEncounterFrameInfoLootScrollFrameButton%

	if (objName1 == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_EncounterJournal"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_EncounterJournal_OnClick);
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
	end--while

	LinksInChat["CALLBACK_Blizzard_EncounterJournal"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_Blizzard_TradeSkillUI()
	--We need to make several hooks for the Tradeskill UI. One for each reagent in the UI.
	local CONST1, i = "TradeSkillReagent", 1;
	local objName1 = _G[CONST1..i]; --TradeSkillReagent%

	if (objName1 == nil) then
		local callback = LinksInChat["CALLBACK_Blizzard_TradeSkillUI"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objName1 ~= nil) do
		objName1:HookScript("OnClick", LinksInChat_TradeSkillReagent_OnClick);
		i = i +1; --Increment counter
		objName1 = _G[CONST1..i];
	end--while

	objName1 = _G["TradeSkillSkillIcon"];
	if (objName1 ~= nil) then objName1:HookScript("OnClick", LinksInChat_TradeSkillReagent_OnClick); end --Use the same eventhandler


	LinksInChat["CALLBACK_Blizzard_TradeSkillUI"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


local cache_BagnonList = nil; --Index of the highest Bagnon button that already have a hook installed.
function LinksInChat:CALLBACK_Bagnon()
	--Hook into Bagnon bag
	--These buttons are not available until the user opens the inventory and bank, for the first time. Need therefore to check for their existense.
	--Guildbank and Voidstorage uses their own sub-addons, different buttonnames and are only needed to be done once.
	local CONST1, i = "BagnonItemSlot", 1; --BagnonItemSlot%
	if (cache_BagnonList ~= nil) then i = cache_BagnonList +1; end --Make loop faster by skipping over older items
	local objItem = _G[CONST1..i];

	while (objItem ~= nil) do
		objItem:HookScript("OnClick", LinksInChat_BagnonItemSlot_OnClick);
		cache_BagnonList = i; --Set index after button has been hooked
		i = i +1; --Increment counter
		objItem = _G[CONST1..i];
	end--while

	local callback = LinksInChat["CALLBACK_Bagnon"];
	C_Timer.After(1, callback); --Sleep and re-check again in 1 second
	return nil;
end


function LinksInChat:CALLBACK_BagnonGuildBank()
	--Hook into Bagnon guildbank bag
	--These buttons are not available until the user opens the guildbank and the addon is then loaded. Need therefore to check for their existense.
	local CONST1, i = "BagnonGuildItemSlot", 1; --BagnonGuildItemSlot%
	local objItem = _G[CONST1..i];

	if (objItem == nil) then
		local callback = LinksInChat["CALLBACK_BagnonGuildBank"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objItem ~= nil) do
		objItem:HookScript("OnClick", LinksInChat_BagnonItemSlot_OnClick);
		i = i +1; --Increment counter
		objItem = _G[CONST1..i];
	end--while

	LinksInChat["CALLBACK_BagnonGuildBank"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end


function LinksInChat:CALLBACK_BagnonVoidStorage()
	--Hook into Bagnon voidstorage bag
	--These buttons are not available until the user opens the guildbank and the addon is then loaded. Need therefore to check for their existense.
	local CONST1, i = "BagnonVaultSlot", 1; --BagnonVaultSlot%
	local objItem = _G[CONST1..i];

	if (objItem == nil) then
		local callback = LinksInChat["CALLBACK_BagnonVoidStorage"];
		C_Timer.After(1, callback); --Trigger function again in 1 second
		return nil;
	end--if

	while (objItem ~= nil) do
		objItem:HookScript("OnClick", LinksInChat_BagnonItemSlot_OnClick);
		i = i +1; --Increment counter
		objItem = _G[CONST1..i];
	end--while

	LinksInChat["CALLBACK_BagnonVoidStorage"] = nil; --Cleanup this function when it's been run sucessfully. Only needs to be done once.
	return nil;
end



--####################################################################################
--####################################################################################
--Settings
--####################################################################################


--Returns the current value for a setting.
function LinksInChat:GetCurrentSetting(strSetting)
	if (strSetting == nil) then return nil end
	strSetting = strupper(strSetting);
	return LINKSINCHAT_settings[strSetting];
end


--Sets the current value for a setting.
function LinksInChat:SetCurrentSetting(strSetting, objValue)
	if (strSetting == nil) then return nil end
	strSetting = strupper(strSetting);
	LINKSINCHAT_settings[strSetting] = objValue;
	return objValue;
end


--####################################################################################
--####################################################################################
--Settings frame
--####################################################################################


function LinksInChat:SettingsFrame_OnLoad(panel)
	-- Set the name of the Panel
	panel.name = Locale["CopyFrame Title"];--"LinksInChat";
	panel.default	= function (self) end; --So few settings that we simply ignore the reset button

	--Add the panel to the Interface Options
	InterfaceOptions_AddCategory(panel);
end


--Called after settings related to search provider has been changed
function LinksInChat:UpdateProvider()
	--Get current settings
	local s_English			= self:GetCurrentSetting("AlwaysEnglish") or false;
	local s_Provider		= self:GetCurrentSetting("Provider") or "wowhead";

	--Validate search provider
	SearchProvider:InitializeProvider(s_English);
	if (SearchProvider:ProviderExists(s_Provider) ~= true) then s_Provider = "wowhead"; end
	cache_Provider = SearchProvider:GetProvider(s_Provider);

	return nil;
end


--####################################################################################
--####################################################################################
--Settings frame - Color picker
--####################################################################################


function LinksInChat:ShowColorPicker(objTexture)
	local strColor	= self:GetCurrentSetting("Color");
	local r,g,b		= self:HexColorToRGBPercent(strColor);

	local cf = ColorPickerFrame;
	cf:SetColorRGB(r,g,b);
	cf.hasOpacity = false; --We dont have Alpha for link colors.
	cf.opacity = 1;

	local f = function() end;
	local o = function() LinksInChat:ColorPicker_Callback("ok", objTexture) end;
	local c = function() LinksInChat:ColorPicker_Callback("cancel", objTexture) end;
	cf.func, cf.opacityFunc, cf.cancelFunc = f, o, c;
	cf:Hide(); -- Need to run the OnShow handler.
	cf:Show();
	return nil;
end


function LinksInChat:ColorPicker_Callback(restore, objTexture)
	local cf = ColorPickerFrame;

	if (restore == "ok") then --'ok' or 'cancel'
		local r,g,b = cf:GetColorRGB();
		--local a = OpacitySliderFrame:GetValue();
		objTexture:SetTexture(r,g,b,1);

		cache_Color_HyperLinks = self:RGBPercentToHex(r,g,b); --HEX formatted string
		self:SetCurrentSetting("Color", cache_Color_HyperLinks);
	end--if restore

	--Cleanup
	local f = function() end;
	cf.func, cf.opacityFunc, cf.cancelFunc = f,f,f;
	return nil;
end


--Takes a RGB percent set (0.0-1.0) and converts it to a hex string.
function LinksInChat:RGBPercentToHex(r,g,b)
	--Source: http://www.wowwiki.com/RGBPercToHex
	r = r <= 1 and r >= 0 and r or 0;
	g = g <= 1 and g >= 0 and g or 0;
	b = b <= 1 and b >= 0 and b or 0;
	return strupper(format("%02x%02x%02x", r*255, g*255, b*255));
end


--Returns r, g, b for a given hex colorstring
function LinksInChat:HexColorToRGBPercent(strHexColor)
	--Expects: RRGGBB  --Red, Green, Blue
	if (strlen(strHexColor) ~= 6) then return nil end
	local tonumber	= tonumber; --local fpointer
	local strsub	= strsub;

	local r, g, b = (tonumber( strsub(strHexColor,1,2), 16) /255), (tonumber( strsub(strHexColor,3,4), 16) /255), (tonumber( strsub(strHexColor,5,6), 16) /255);
	if (r==nil or g==nil or b==nil) then return nil end
	return r, g, b;
end


--####################################################################################
--####################################################################################
--Settings frame - Dropdown menus
--####################################################################################
--Source: InterfaceOptionsPanel.lua, InterfaceOptionsPanel.xml ($parentAutoLootKeyDropDown)


function LinksInChat:Settings_Frame_DropDown_AutoHide_OnEvent(objSelf, event, ...)
	if ( event == "PLAYER_ENTERING_WORLD" ) then
		objSelf.defaultValue = -1;
		objSelf.oldValue = self:GetCurrentSetting("AutoHide");
		objSelf.value = objSelf.oldValue or objSelf.defaultValue;

		local init = function(...) return self.Settings_Frame_DropDown__AutoHide_Initialize(self,...) end; --This function is called each time the Dropdown menu is clicked

		UIDropDownNoTaintMenu_SetWidth(objSelf, 150);
		UIDropDownNoTaintMenu_Initialize(objSelf, init);
		UIDropDownNoTaintMenu_SetSelectedValue(objSelf, objSelf.value);

		objSelf.SetValue		=	function(objSelf, value)
										objSelf.value = value
										UIDropDownNoTaintMenu_SetSelectedValue(objSelf, value)
										self:SetCurrentSetting("AutoHide", value)
									end;
		objSelf.GetValue		=	function(objSelf)
										return UIDropDownNoTaintMenu_GetSelectedValue(objSelf)
									end;
		objSelf.RefreshValue	=	function (objSelf)
										UIDropDownNoTaintMenu_Initialize(objSelf, init)
										UIDropDownNoTaintMenu_SetSelectedValue(objSelf, objSelf.value)
									end;
		objSelf:UnregisterEvent(event);
	end--if event
end

function LinksInChat_Settings_Frame_DropDown_AutoHide_OnClick(self)
	--We declare this one here. If we declared it inline in :Settings_Frame_DropDown__AutoHide_Initialize(), it would mean that every time that the user clicked the dropdown it would generate a new and wasted function pointer
	LinksInChatXML_Settings_Frame_DropDown_AutoHide:SetValue(self.value);
end

function LinksInChat:Settings_Frame_DropDown__AutoHide_Initialize()
	local selectedValue = UIDropDownNoTaintMenu_GetSelectedValue(LinksInChatXML_Settings_Frame_DropDown_Provider);
	local info = UIDropDownNoTaintMenu_CreateInfo();

	local Lsub = Locale["Dropdown Options Autohide"];
	info.text = Lsub["none"]; --"Don't hide";
	info.func = LinksInChat_Settings_Frame_DropDown_AutoHide_OnClick;
	info.value = -1;
	if ( info.value == selectedValue ) then
		info.checked = true;
	else
		info.checked = false;
	end
	UIDropDownNoTaintMenu_AddButton(info);

	info.text = Lsub["3sec"]; --"3 seconds";
	info.func = LinksInChat_Settings_Frame_DropDown_AutoHide_OnClick;
	info.value = 3;
	if ( info.value == selectedValue ) then
		info.checked = true;
	else
		info.checked = false;
	end
	UIDropDownNoTaintMenu_AddButton(info);

	info.text = Lsub["5sec"]; --"5 seconds";
	info.func = LinksInChat_Settings_Frame_DropDown_AutoHide_OnClick;
	info.value = 5;
	if ( info.value == selectedValue ) then
		info.checked = true;
	else
		info.checked = false;
	end
	UIDropDownNoTaintMenu_AddButton(info);

	info.text = Lsub["7sec"]; --"7 seconds";
	info.func = LinksInChat_Settings_Frame_DropDown_AutoHide_OnClick;
	info.value = 7;
	if ( info.value == selectedValue ) then
		info.checked = true;
	else
		info.checked = false;
	end
	UIDropDownNoTaintMenu_AddButton(info);

	info.text = Lsub["10sec"]; --"10 seconds";
	info.func = LinksInChat_Settings_Frame_DropDown_AutoHide_OnClick;
	info.value = 10;
	if ( info.value == selectedValue ) then
		info.checked = true;
	else
		info.checked = false;
	end
	UIDropDownNoTaintMenu_AddButton(info);
end


function LinksInChat:Settings_Frame_DropDown_Provider_OnEvent(objSelf, event, ...)
	if ( event == "PLAYER_ENTERING_WORLD" ) then
		objSelf.defaultValue = "wowhead";
		objSelf.oldValue = self:GetCurrentSetting("Provider");
		objSelf.value = objSelf.oldValue or objSelf.defaultValue;

		local init = function(...) return self.Settings_Frame_DropDown__Provider_Initialize(self,...) end; --This function is called each time the Dropdown menu is clicked

		UIDropDownNoTaintMenu_SetWidth(objSelf, 220);
		UIDropDownNoTaintMenu_Initialize(objSelf, init);
		UIDropDownNoTaintMenu_SetSelectedValue(objSelf, objSelf.value);

		objSelf.SetValue		=	function(objSelf, value)
										objSelf.value = value
										UIDropDownNoTaintMenu_SetSelectedValue(objSelf, value)
										self:SetCurrentSetting("Provider", value)
										self:UpdateProvider()
									end;
		objSelf.GetValue		=	function(objSelf)
										return UIDropDownNoTaintMenu_GetSelectedValue(objSelf)
									end;
		objSelf.RefreshValue	=	function (objSelf)
										UIDropDownNoTaintMenu_Initialize(objSelf, init)
										UIDropDownNoTaintMenu_SetSelectedValue(objSelf, objSelf.value)
									end;
		objSelf:UnregisterEvent(event);
	end--if event
end

function LinksInChat_Settings_Frame_DropDown_Provider_OnClick(self)
	--We declare this one here. If we declared it inline in :Settings_Frame_DropDown__Provider_Initialize(), it would mean that every time that the user clicked the dropdown it would generate a new and wasted function pointer
	LinksInChatXML_Settings_Frame_DropDown_Provider:SetValue(self.value);
end

function LinksInChat:Settings_Frame_DropDown__Provider_Initialize()
	local selectedValue = UIDropDownNoTaintMenu_GetSelectedValue(LinksInChatXML_Settings_Frame_DropDown_Provider);
	local info = UIDropDownNoTaintMenu_CreateInfo();

	local tblProviders, tblSorted = SearchProvider:GetProvider("all");

	for i = 1, #tblSorted do --tblSorted gives us a sorted list of the key's
		local providerKey	= strlower(tostring(tblSorted[i])); --key = "provider uniqe name", data = table with localized provider data
		local title			= tblProviders[providerKey]["Title"];
		info.text = title;
		info.func = LinksInChat_Settings_Frame_DropDown_Provider_OnClick;
		info.value = providerKey;
		if ( info.value == selectedValue ) then
			info.checked = true;
		else
			info.checked = false;
		end
		UIDropDownNoTaintMenu_AddButton(info);
	end--for i
	return nil;
end


--####################################################################################
--####################################################################################
--Copy link frame
--####################################################################################


--Callback used to iterate on the CopyFrame
local TimerTicker		= nil;	--nil or pointer to a ticker object.
local TimerIteration	= 0;	--Counts down from X until 0

function LinksInChat:CopyFrame_TimerTick()
	TimerIteration = TimerIteration -1;
	LinksInChat_Copy_Frame_Countdown:SetText(TimerIteration);

	if (TimerIteration <= 0) then --We only execute this code once to hide the frame
		LinksInChat_Copy_Frame:Hide(); --Hide the frame
		LinksInChat_Copy_Frame_Countdown:SetText("");
		TimerIteration = 0;
		TimerTicker = nil;
		collectgarbage("collect"); --force a garbage collection
	end	--if
	return nil;
end


function LinksInChat:CopyFrame_Show(linkType, text, link)
	--linkType: either 'wcl' or 'other'. If it's wcl then its a www or http:// link and we use text. Otherwise its a hyperlink and we must use that
	--returns: true if the linktype is unknown and the link should be propagated further. otherwise false.
	local message, res = "", true;

	if (linkType == "wcl") then
		message = self:WebLink_Strip(text);
		res = false;
	else
		if (self:GetCurrentSetting("IgnoreHyperlinks") == true) then return true; end --Return immediatly if we are to ignore hyperlinks all together
		message = LinkWebParsing:getHyperLinkURI(link, text, cache_Provider, self:GetCurrentSetting("Simple"), self:GetCurrentSetting("UseHTTPS") ); --Returns an string or nil.
		if (message == nil) then return true; end --Will be nil if we do not support the hyperlink.
		res = false;
	end--if linkType

	--Display frame with pre-selected uri ready to be copied to the clipboard
	LinksInChat_Copy_Frame:Hide();
	LinksInChat_Copy_Frame_Countdown:SetText("");
	LinksInChat_Copy_Frame_EditBox:SetText(message);
	LinksInChat_Copy_Frame:Show();
	LinksInChat_Copy_Frame_EditBox:SetFocus();

	--Create a timer-ticker that will get invokes every second until the Display is to be hidden.
	local sec = tonumber(self:GetCurrentSetting("AutoHide")); --Will count down from X until 0. Then the form will hide itself.
	if (sec ~= nil and sec > 0) then
		if (TimerTicker ~= nil) then TimerTicker:Cancel(); end --Cancel any potentially still running timer

		LinksInChat_Copy_Frame_Countdown:SetText(tostring(sec));
		TimerIteration = sec;
		local callback = self["CopyFrame_TimerTick"];
		TimerTicker = C_Timer.NewTicker(1, callback, sec);
	end--if sec
	return res;
end


function LinksInChat:CopyFrame_ExtraShow(link)
	--Subfunction used by alot of hooks since most of their code identical
	if (link == nil)										then return false; end --if link is nil then its a empty slot.
	if (IsAltKeyDown()==0 or IsAltKeyDown()==false)			then return false; end --If alt-key isnt pressed then skip this.
	if (LinksInChat:GetCurrentSetting("Extra") == false)	then return false; end
	ClearCursor();

	local data, text = LinksInChat:HyperLink_Strip2(link);
	LinksInChat:CopyFrame_Show("other", text, data); --Show the text of the link
	return true;
end


--####################################################################################
--####################################################################################
--Custom hyperlink handling
--####################################################################################


--Returns a wcl: hyperlink in the correct format
function LinksInChat:HyperLink_Create(title)
	--Format: |cFF000000|Hwcl:|h[title]|h|r
	local res = "|cFF@COLOR@|Hwcl:|h@TITLE@|h|r";
	res = LinkWebParsing:replace(res, "@COLOR@", cache_Color_HyperLinks);
	res = LinkWebParsing:replace(res, "@TITLE@", tostring(title) ); --Manually add [ ] if you want that as part of the title
	return res;
end


--Removes any weblink data from the string
function LinksInChat:WebLink_Strip(link)
	--[[
		Weblinks can be inside Weblinks (www. inside http://)
		Hyperlinks can not be inside weblinks
		"Hello|cFF000000http://www.link.com|h|r|cff000000[item]|h|rWorld"
	]]--
	local link2 = LinkWebParsing:replace(link, "|cFF"..tostring(cache_Color_HyperLinks).."|Hwcl:|h", ""); --plain replace of the whole beginning of the string
	local link2 = LinkWebParsing:replace(link, "|cFF"..tostring(cache_Color_HyperLinks).."|Hwcl:|h", ""); --plain replace of the whole beginning of the string
	if (link2 ~= link) then
		link2 = LinkWebParsing:replace(link2, "|h|r", ""); --plain replace at the end of the string
		return link2;
	end
	return link;
end


--Returns the plain text title of a web-link (http:// etc)
function LinksInChat:HyperLink_Strip(link)
	local p = "%|h(.-)%|h%|r"; --Just the text inside the hyperlink (note we dont use the [ ] in web-links
	local startPos, endPos, firstWord, restOfString = strfind(link, p);
	if (firstWord ~= nil) then return firstWord; end
	return nil;
end


--Returns the linkdata and plain text of a hyperlink
function LinksInChat:HyperLink_Strip2(link)
	local p = "%H(.-)%|h%[(.-)%]%|h%|r"; --Just the text inside the hyperlink (note that we use [ ] here)
	local startPos, endPos, firstWord, restOfString = strfind(link, p);
	if (firstWord ~= nil) then return firstWord, restOfString; end
	return nil;
end


--####################################################################################
--####################################################################################
--Hooks
--####################################################################################


--This hook makes us able to create and handle customized hyperlinks for the addon
function LinksInChat_ChatFrame_OnHyperlinkShow(chatframe, link, text, button) --function ChatFrame_OnHyperlinkShow(chatframe, link, text, button)
	--Hook is done in LinksInChat:OnEvent()
	local start = strfind(link, "wcl:", 1, true); --plain find starting at pos 1 in the string
	if (start == 1) then --This is a web-link (not a hyperlink)
		if (IsShiftKeyDown()==1 or IsShiftKeyDown()==true) then
			--Shift was held while clicking the web-link, we pass it along like a normal string (if the editbox is visible at the moment)...
			local t = strtrim(LinksInChat:HyperLink_Strip(text));
			if (t ~= nil) then
				local n = chatframe:GetName().."EditBox";
				local f = _G[n];
				--If the user presses Shift, we insert the web-link in plaintext into the chatframe's editbox.
				if (f ~= nil) then
					if (f:IsVisible() == 1) then
						f:Insert(" "..t.." "); --Editbox already open
					else
						f:Show();
						f:Insert(t.." ");
						f:SetFocus();
					end--if f:isVisible()
				end---if f
			end--if t

		else
			--Shift was not held down
			LinksInChat:CopyFrame_Show("wcl", text, nil); --Show the text of the link
		end--if IsShiftKeyDown

		return nil; --Stop propagation of all wcl: web-links
	else --This is a Hyperlink
		if (IsAltKeyDown()==1 or IsAltKeyDown()==true) then
			--If Alt was held while clicking the hyperlink we will show the copy window too (maybe; depending on the link)
			local propagate = LinksInChat:CopyFrame_Show("other", text, link); --Show the text of the link
			if (propagate == false) then return nil; end --Stop further propagation of this hyperlink (we have shown the copy frame for it)

			--If the Hyperlink-type is of some unknown type (maybe some custom addon hyperlink??), we will just leave it alone...
			return hook_ChatFrame_OnHyperlinkShow(chatframe, link, text, button);
		end--if IsAltKeyDown

		--Regular hyperlink without ALT beign held. Pass it along.
		return hook_ChatFrame_OnHyperlinkShow(chatframe, link, text, button);
	end--if start
end


--This hook makes us able to hook into the questlog
function LinksInChat_QuestMapLogTitleButton_OnClick(s, button) --function QuestMapLogTitleButton_OnClick(self, button)
	--Hook is done in LinksInChat:OnEvent()
	--2014-12-07 Known issue: Clicking the 1st entry in the questlog does not work.
	local link = GetQuestLink(GetQuestLogIndexByID(s.questID));
	local b = LinksInChat:CopyFrame_ExtraShow(link);
	if (b == true) then return nil; end --Skip passing hook if we show the link.

	return hook_QuestMapLogTitleButton_OnClick(s, button); --Call original function
end


--This hook makes us able to hook into the objective tracker (achievement & quest part)
function LinksInChat_ACHIEVEMENT_TRACKER_MODULE(s, block, mouseButton)
	--Hook is done in LinksInChat:OnEvent()
	--Source: Blizzard_AchievementObjectiveTracker.lua
	local link = GetAchievementLink(block.id);
	local b = LinksInChat:CopyFrame_ExtraShow(link);
	if (b == true) then return nil; end --Skip calling original function if we show the link.

	return hook_ACHIEVEMENT_TRACKER_MODULE(s, block, mouseButton); --Call original function
end
function LinksInChat_QuestLogPopupDetailFrame_Show(questLogIndex)
	--Post-hook on the questlog details frame.
	--Source: FrameXML\QuestMapFrame.lua
	if (QuestLogPopupDetailFrame:IsShown() == false) then return nil; end
	local link = GetQuestLink(questLogIndex);
	local b = LinksInChat:CopyFrame_ExtraShow(link);
	if (b == true) then QuestLogPopupDetailFrame:Hide(); end
end


--Event handler for Button.OnClick for Character and Inspect frame
function LinksInChat_CharacterPaperDoll_OnClick(objButton, strButton)
	if (IsAltKeyDown()==0 or IsAltKeyDown()==false) then return nil; end --If alt-key isnt pressed then skip this.
	if (LinksInChat:GetCurrentSetting("Extra") ~= true) then return nil; end
	if (strButton ~= "LeftButton") then return nil; end

	local slotName = objButton:GetName(); ---Button name
	local strUnit = "player";
	if (LinkWebParsing:indexOf(slotName, "Inspect", 1) ~= nil) then strUnit = "target"; end --If we are doing an /inspect we will get links from our current target

	slotName = LinkWebParsing:replace(slotName, "Character", "");	--Remove button name prefix, rest matches the slotname
	slotName = LinkWebParsing:replace(slotName, "Inspect", "");
	local slotID = GetInventorySlotInfo(slotName);					--Return numeric id for the slot

	local link = GetInventoryItemLink(strUnit, slotID);	--Get the itemlink for the item in question
	if (link == nil) then return nil; end				--Might be an empty slot

	--ClearCursor();
	local data, text = LinksInChat:HyperLink_Strip2(link);
	LinksInChat:CopyFrame_Show("other", text, data);		--Show the text of the link
	return nil;
end


--Event handler for Button.Onclick for AchievementButton_OnClick
function LinksInChat_AchievementButton_OnClick(s, button, down, ignoreModifiers)
	if (IsAltKeyDown()==1 or IsAltKeyDown()==true) then --If alt-key isnt pressed then skip this.
		if (LinksInChat:GetCurrentSetting("Extra") == true) then
			local id, text = GetAchievementInfo(s.id);
			if (id ~= nil) then
				local data = "achievement:"..id..":"..UnitGUID("player")..":0:0:0:0:0:0:0:0"; --Create a ad-hock achievement link
				LinksInChat:CopyFrame_Show("other", text, data); --Show the text of the link
			end--if
		end--if GetCurrentSetting
	end--if IsAltKeyDown
	return true; --This is a post-hook.
end


--Event handler for BrowseButtonTemplateItem.OnClick widget for the Auction house UI.
function LinksInChat_BrowseButtonTemplateItem_OnClick(objButton, button, down)
	return LinksInChat_BrowseButtonTemplate_OnClick(objButton:GetParent(), button, down); --Same code as LinksInChat_BrowseButtonTemplate_OnClick, Just need to refer the the parent.
end
--Event handler for BrowseButtonTemplate.OnClick widget for the Auction house UI.
function LinksInChat_BrowseButtonTemplate_OnClick(objButton, button, down)
	--We can't hook into the BrowseButton_OnClick() found in "Blizzard_AuctionUI.lua" since that is triggered after the widget's script handler, and that got some logic regarding IsModifiedClick() in it that will trump the check for IsAltKeyDown()
	--Therefore this function is hooked into the OnClick part of the BrowseButtonTemplate's <script> found in Blizzard_AuctionUITemplates.xml
	local link = GetAuctionItemLink("list", objButton:GetID() + FauxScrollFrame_GetOffset(BrowseScrollFrame)); --Source: Blizzard_AuctionUI.lua
					--Some addons like Auctioneer will change the functionality of the AH and GetAuctionItemLink can then return nil because of that
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for BlackMarketItemTemplate.Item.OnClick widget for the Black Market UI.
function LinksInChat_BlackMarketItemTemplateItem_OnClick(objButton, button, down)
	return LinksInChat_BlackMarketItemTemplate_OnClick(objButton:GetParent(), button, down); --Same code as LinksInChat_BlackMarketItemTemplate_OnClick, Just need to refer the the parent.
end
function LinksInChat_BlackMarketItemTemplate_OnClick(objButton, button, down)
	--Same issue here as with the Auction House UI. Must therefore hook into widget.
	--Source: Blizzard_BlackMarketUI.xml
	local link = objButton.itemLink;
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for ToyBox.OnClick widget for the ToyBox UI.
function LinksInChat_ToySpellButton_OnClick(s, button)
	--Source: Blizzard_PetJournal.xml
	local link = C_ToyBox.GetToyLink(s.itemID);
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for PetJournalListScrollFrameButtonN widget for the PetJournal UI.
function LinksInChat_PetJournalListScrollFrameButton_dragButton_OnClick(s, button)
	return LinksInChat_PetJournalListScrollFrameButton_OnClick(s:GetParent(), button);
end
function LinksInChat_PetJournalListScrollFrameButton_OnClick(s, button)
	--Source: Blizzard_PetJournal.xml
	if (IsAltKeyDown()==1 or IsAltKeyDown()==true) then --If alt-key isnt pressed then skip this.
		if (LinksInChat:GetCurrentSetting("Extra") == true) then
			local id, data, text = s.petID, nil, nil;
			if (id ~= nil) then	--The pet has been collected. We can get a link for it and use that
				local link = C_PetJournal.GetBattlePetLink(id);
				data, text = LinksInChat:HyperLink_Strip2(link);
			else				--Pet has not been collected yet, We can lookup its localized name using the speciesID.
				text = C_PetJournal.GetPetInfoBySpeciesID(s.speciesID);
			end
			LinksInChat:CopyFrame_Show("other", text, data); --Show the text of the link
		end--if GetCurrentSetting
	end--if IsAltKeyDown
	return true; --This is a post-hook.
end


--Event handler for MountJournalListScrollFrameButtonN widget for the MountJournal UI.
function LinksInChat_MountJournalListScrollFrameButton_DragButton_OnClick(s, button)
	return LinksInChat_MountJournalListScrollFrameButton_OnClick(s:GetParent(), button);
end
function LinksInChat_MountJournalListScrollFrameButton_OnClick(s, button)
	--Source: Blizzard_PetJournal.xml
	local link = GetSpellLink(s.spellID);
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for Encounter Journal loot list buttons
function LinksInChat_EncounterJournal_OnClick(s, button)
	--Source: Blizzard_EncounterJournal.xml
	local link = s.link;
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for TradeskillReagent_OnClick for the default Blizzard tradeskill UI
function LinksInChat_TradeSkillReagent_OnClick(s, button)
	local link = GetTradeSkillReagentItemLink(TradeSkillFrame.selectedSkill, s:GetID()) or GetTradeSkillItemLink(TradeSkillFrame.selectedSkill); --If GetTradeSkillReagentItemLink returns nil then its got to be the 'TradeSkillSkillIcon' that is clicked and we use GetTradeSkillItemLink
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for LootButton_OnClick for the default Blizzard lootbags
function LinksInChat_LootButton_OnClick(s, button)
	local link = GetLootSlotLink(s.slot);
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for GroupLootFrame.IconFrame_OnClick for the default Blizzard grouploot (the frame with the need/greed/diss buttons)
function LinksInChat_GroupLootFrame_OnClick(s, button)
	local link = GetLootRollItemLink(s:GetParent().rollID);
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for VoidStorageItemButton_OnClick for the default Blizzard void storage
function LinksInChat_VoidStorageItemButton_OnClick(s, button)
	--Source: 2014-11-17: Blizzard_VoidStorageUI.lua.
	local itemID, link, _, BUTTON_TYPE_DEPOSIT, BUTTON_TYPE_WITHDRAW, BUTTON_TYPE_STORAGE = nil, nil, nil, 1, 2, 3;
	if (s.buttonType == BUTTON_TYPE_DEPOSIT)		then itemID = GetVoidTransferDepositInfo(s.slot);
	elseif (s.buttonType == BUTTON_TYPE_STORAGE)	then itemID = GetVoidItemInfo(VoidStorageFrame.page, s.slot);
	elseif (s.buttonType == BUTTON_TYPE_WITHDRAW)	then itemID = GetVoidTransferWithdrawalInfo(s.slot); end
	if (itemID ~= nil) then _, link = GetItemInfo(itemID); end

	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for GuildBankItem_OnClick for the default Blizzard guildbank
function LinksInChat_GuildBankItem_OnClick(s, button)
	local link = GetGuildBankItemLink(GetCurrentGuildBankTab(), s:GetID());
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for ContainerFrameItem_OnClick for the default Blizzard bags, bankframe and reagent bankframe
function LinksInChat_ContainerFrameItem_OnClick(s, button)
	local link = GetContainerItemLink(s:GetParent():GetID(), s:GetID());
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for MerchantItemButton_OnClick for the default Blizzard merchantframe
function LinksInChat_MerchantItemButton_OnClick(s, button)
	local link = GetMerchantItemLink(s:GetID());
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--Event handler for Bagnon addon and Bagnon_GuildBank
function LinksInChat_BagnonItemSlot_OnClick(s, button)
	local link = s:GetItem() --Bagnon function
	LinksInChat:CopyFrame_ExtraShow(link);
	return true; --This is a post-hook.
end


--####################################################################################
--####################################################################################
