------------------------------------------------------------------------
r135 | slippycheeze | 2015-01-24 07:08:35 +0000 (Sat, 24 Jan 2015) | 1 line
Changed paths:
   A /tags/60000.18 (from /trunk:133)

update to latest BSF library
------------------------------------------------------------------------
r133 | slippycheeze | 2015-01-17 05:41:37 +0000 (Sat, 17 Jan 2015) | 1 line
Changed paths:
   M /trunk/src/Rotations.lua
   M /trunk/src/Spells.lua

Remove some dead locals
------------------------------------------------------------------------
r132 | slippycheeze | 2015-01-17 05:41:33 +0000 (Sat, 17 Jan 2015) | 1 line
Changed paths:
   M /trunk/src/Spells.lua

Fix Power Infusion cooldown, thanks Jobeo
------------------------------------------------------------------------
r128 | slippycheeze | 2014-12-27 00:55:31 +0000 (Sat, 27 Dec 2014) | 1 line
Changed paths:
   M /trunk/src/Spells.lua

Fix CoP and Insanity causing Devouring Plague to flash with < 3 orbs
------------------------------------------------------------------------
r126 | slippycheeze | 2014-12-24 17:43:24 +0000 (Wed, 24 Dec 2014) | 1 line
Changed paths:
   M /trunk/src/Spells.lua

Fix lua error / crash with Insanity talent
------------------------------------------------------------------------
