## Interface: 60000
## Version: 60000.18
## Author: SlippyCheez, Xemnosyst
## Title: Bitten's SpellFlash: Priest
## Notes: Replaces Blizzard's default proc highlighting to flash 1) a maximum dps rotation, or 2) more informative healing proc highlights.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-priest
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Priest
## X-Curse-Packaged-Version: 60000.18
## X-Curse-Project-Name: Bitten's SpellFlash: Priest
## X-Curse-Project-ID: bittens-spellflash-priest
## X-Curse-Repository-ID: wow/bittens-spellflash-priest/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
